import { StatusBar } from 'expo-status-bar';
import React, { useReducer, useEffect } from 'react';
import { StyleSheet, Text, View, Button } from 'react-native';
import ScheduleScreen from './src/screens/ScheduleScreen/ScheduleScreen';
import LoginScreen from './src/screens/LoginScreen';
import Navigation from './src/navigation/Navigation';
import useUser from './src/peregrine/talons/App/useUser';
import styles from './src/screens/ScheduleScreen/style';
import AppContext from './src/peregrine/store/context/appContext';

function reducer(state, action, dispatch) {
  // console.log({ state });
  console.log({ dispatch });
  switch (action.type) {
    case 'SET_USER':
      state.user = action.data;
      return {
        // user: action.data,
        ...state,
      };
    case 'SIGN_OUT':
      state.user = null;
      return { ...state };
    default:
      throw new Error();
  }
}
export default function App() {
  const [state, dispatch] = useReducer(reducer, { user: null });

  return (
    <AppContext.Provider value={{ state, dispatch }}>
      <StatusBar animated={true} backgroundColor='white' />
      {state.user ? <Navigation /> : <LoginScreen />}
    </AppContext.Provider>
  );
}
